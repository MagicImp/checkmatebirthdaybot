using CMBirthdayBot.Database;
using CMBirthdayBot.Discord.Implementations;
using CMBirthdayBot.Discord.TypeParsers;
using CMBirthdayBot.Services;
using Disqord;
using Disqord.Bot.Hosting;
using Disqord.Extensions.Interactivity;
using Disqord.Gateway;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CMBirthdayBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        private static async Task MainAsync(string[] args)
        {
            IHost host = CreateHostBuilder(args).Build();

            try
            {
                await UpdateDatabaseMigrations(host);

                await host.RunAsync();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Startup failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return new HostBuilder()
                .ConfigureAppConfiguration(x =>
                {
                    x.AddJsonFile("./appsettings.json");
                    x.AddJsonFile("./DateTimeFormats.json");
                    x.AddJsonFile("./appsettings.Development.json", true);
                })
                .ConfigureLogging((hostContext, logBuilder) =>
                {
                    var logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(hostContext.Configuration)
                        .CreateLogger();

                    Log.Logger = logger;

                    logBuilder.ClearProviders();
                    logBuilder.AddSerilog(logger, true);
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddInteractivity();
                    services.AddSingleton<DateTimeTypeParser>();
                    services.AddDbContext<ApplicationDbContext>(x => x.UseSqlite(context.Configuration.GetConnectionString("sqlite")));

                    services.AddSingleton<BirthdayAnnouncementService>();
                    services.AddHostedService(x => x.GetService<BirthdayAnnouncementService>());
                })
                .ConfigureDiscordBotSharder<CheckmateBotSharder>((context, bot) =>
                {
                    bot.Token = context.Configuration["Discord:Token"];
                    bot.ReadyEventDelayMode = ReadyEventDelayMode.Guilds;
                    bot.UseMentionPrefix = true;
                    bot.Prefixes = context.Configuration.GetSection("Discord:Prefix").Get<string[]>();
                    bot.Intents += GatewayIntent.Members | GatewayIntent.GuildMessages;
                });
        }

        private static async Task UpdateDatabaseMigrations(IHost host)
        {
            using var scopeContext = host.Services.CreateScope();
            var context = scopeContext.ServiceProvider.GetService<ApplicationDbContext>();

            var pendingMigrations = await context.Database.GetPendingMigrationsAsync();
            if (pendingMigrations.Any())
                await context.Database.MigrateAsync();
        }
    }
}
