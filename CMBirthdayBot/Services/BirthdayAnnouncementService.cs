﻿using CMBirthdayBot.Database;
using CMBirthdayBot.Discord.Implementations;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Timers = System.Timers;

using Disqord;
using Disqord.Rest;
using Disqord.WebSocket;
using System.Text;

namespace CMBirthdayBot.Services
{
    public class BirthdayAnnouncementService : IHostedService
    {
        public Timers.Timer Timer { get; private set; } = new Timers.Timer();
        private ILogger<BirthdayAnnouncementService> _logger;
        private CheckmateBotSharder _checkmateBot;
        private ApplicationDbContext _db;
        private ulong _channelId;


        public BirthdayAnnouncementService(ILogger<BirthdayAnnouncementService> logger, CheckmateBotSharder checkmateBot, ApplicationDbContext db, IConfiguration configuration)
        {
            _channelId = configuration.GetValue<ulong>("Discord:AnnouncementChannelId");
            _checkmateBot = checkmateBot;
            _logger = logger;
            _db = db;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting BirthdayAnnouncementService");
            TimeSpan nextExecuteTime = GetTimeSpanToNextAnnouncement();
            _logger.LogInformation("Next birthday announcement gets executed in {0}", nextExecuteTime.ToString(@"hh\:mm\:ss"));
            Timer.Interval = nextExecuteTime.TotalMilliseconds;
            Timer.Elapsed += TimerElapsed;
            Timer.AutoReset = true;
            Timer.Enabled = true;

            return Task.CompletedTask;
        }

        private async void TimerElapsed(object sender, Timers.ElapsedEventArgs e)
        {
            var date = e.SignalTime.ToUniversalTime().Date;
            var x = _db.Birthdays.Where(x => x.Date.Day == date.Day && x.Date.Month == date.Month);

            if (x.Any())
            {
                var msgString = new StringBuilder();

                foreach (var bDay in x)
                {
                    // • ├
                    msgString.AppendLine(bDay.UserMention);
                    _logger.LogDebug("B-Day: {0} | {1}", bDay.UserMention, bDay.Date);
                }

                var embed = new LocalEmbedBuilder();
                    embed.Title = "🎉 Today's birthday children 🎉";
                    embed.Description = msgString.ToString();

                var msg = new LocalMessageBuilder()
                    .WithEmbed(embed)
                    .Build();

                await _checkmateBot.SendMessageAsync(_channelId, msg);
            }

            Timer.Interval = GetTimeSpanToNextAnnouncement().TotalMilliseconds;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Timer.Dispose();

            return Task.CompletedTask;
        }

        private TimeSpan GetTimeSpanToNextAnnouncement()
        {
            var now = DateTime.UtcNow;
            var nextAnnouncement = DateTime.UtcNow.Date.AddHours(25);
            return nextAnnouncement - now;
        }
    }
}
