﻿using Disqord.Bot;
using Microsoft.Extensions.Logging;
using Qmmands;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CMBirthdayBot.Discord.TypeParsers
{
    public class DateTimeTypeParser : DiscordTypeParser<DateTime>
    {
        private string[] Formats;
        private ILogger<DateTimeTypeParser> _logger;

        public DateTimeTypeParser(IConfiguration configuration, ILogger<DateTimeTypeParser> logger)
        {
            Formats = configuration.GetSection("formats").Get<string[]>();
            _logger = logger;
        }

        public override ValueTask<TypeParserResult<DateTime>> ParseAsync(Parameter parameter, string value, DiscordCommandContext context)
        {
            if(!DateTime.TryParseExact(value, Formats,null, DateTimeStyles.None, out var dateTime))
            {
                _logger.LogWarning("Cloudn't parser {0} to DateTime", value);
                return Failure($"Cloudn't parser {value} to DateTime");
            }
            return Success(dateTime);
        }
    }
}
