﻿using System;
using System.Threading.Tasks;
using CMBirthdayBot.Database;
using CMBirthdayBot.Database.Models;
using CMBirthdayBot.Discord.Implementations;
using Disqord;
using Microsoft.Extensions.Logging;
using Qmmands;

namespace CMBirthdayBot.Discord.Modules
{
    public class BirthdayModule : CheckmateBaseGuildModule
    {
        private ILogger<BirthdayModule> _logger;
        private ApplicationDbContext _db;

        public BirthdayModule(ILogger<BirthdayModule> logger, ApplicationDbContext db)
        {
            _logger = logger;
            _db = db;
        }

        [Command("birthday", "b")]
        public async Task BrithdayAsync(DateTime date)
        {
            await DeleteContextMessageAsync();

            var r = await _db.Birthdays.FindAsync((ulong)Context.Author.Id);

            if(r == null)
            {
                _db.Birthdays.Add(new Birthday { UserId = Context.Author.Id, Date = date });
                await _db.SaveChangesAsync();
                SelfDestructiveResponse("Birthday saved", TimeSpan.FromSeconds(5));
                return;
            }

            if(r.Date != date)
            {
                r.Date = date;
                _db.Birthdays.Update(r);
                await _db.SaveChangesAsync();
                SelfDestructiveResponse("Birthday updated", TimeSpan.FromSeconds(5));
                return;
            }

            _db.Birthdays.Remove(r);
            await _db.SaveChangesAsync();
            SelfDestructiveResponse("Birthday removed", TimeSpan.FromSeconds(5));
        }

        [Command("birthday", "b")]
        public async Task BrithdayAsync(IMember member = null)
        {
            await DeleteContextMessageAsync();
            
            ulong userId = member == null ? Context.Author.Id : member.Id;
            var r = await _db.Birthdays.FindAsync(userId);

            if(r == null)
            {
                await Response("No birthday is stored for this user");
                return;
            }

            await Response($"Birthday: {r.Date:d.M.yyyy}");
        }
    }
}
