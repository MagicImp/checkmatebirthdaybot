﻿using CMBirthdayBot.Discord.TypeParsers;
using Disqord.Bot;
using Disqord.Bot.Sharding;
using Disqord.Sharding;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Qmmands;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace CMBirthdayBot.Discord.Implementations
{
    public class CheckmateBotSharder : DiscordBotSharder
    {
        public CheckmateBotSharder(IOptions<DiscordBotSharderConfiguration> options, ILogger<DiscordBotSharder> logger, IPrefixProvider prefixes, ICommandQueue queue, CommandService commands, IServiceProvider services, DiscordClientSharder client) : base(options, logger, prefixes, queue, commands, services, client)
        {
            commands.AddTypeParser(services.GetService<DateTimeTypeParser>());
        }
    }
}
