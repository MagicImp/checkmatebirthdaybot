﻿using Disqord.Bot;
using System.Threading.Tasks;
using Disqord.Rest;
using Disqord;
using System;

namespace CMBirthdayBot.Discord.Implementations
{
    public class CheckmateBaseGuildModule : DiscordModuleBase<DiscordGuildCommandContext>
    {
        public async Task DeleteContextMessageAsync()
        {
            await Context.Message.DeleteAsync();
        }

        public void SelfDestructiveResponse(string content, TimeSpan time, LocalMentionsBuilder mentions = null)
        {
            _ = Task.Run(async () =>
            {
                var msg = await Response(content, mentions);
                await Task.Delay(time);
                await msg.DeleteAsync();
            });
        }

        public void SelfDestructiveResponse(LocalEmbedBuilder embed, TimeSpan time)
        {
            _ = Task.Run(async () =>
            {
                var msg = await Response(embed);
                await Task.Delay(time);
                await msg.DeleteAsync();
            });
        }

        public void SelfDestructiveResponse(string content, LocalEmbedBuilder embed, TimeSpan time, LocalMentionsBuilder mentions = null)
        {
            _ = Task.Run(async () =>
            {
                var msg = await Response(content, embed, mentions);
                await Task.Delay(time);
                await msg.DeleteAsync();
            });
        }
    }
}
