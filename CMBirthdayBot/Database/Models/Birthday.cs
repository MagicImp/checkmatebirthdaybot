﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMBirthdayBot.Database.Models
{
    public class Birthday
    {
        [Key]
        public ulong UserId { get; set; }
        public DateTime Date { get; set; }
        [NotMapped]
        public string UserMention { get => $"<@{UserId}>"; }
    }
}
