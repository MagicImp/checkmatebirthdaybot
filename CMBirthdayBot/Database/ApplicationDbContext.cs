﻿using CMBirthdayBot.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CMBirthdayBot.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ILogger<ApplicationDbContext> logger, ILoggerFactory loggerFactory) : base(options)
        {
        }

        public DbSet<Birthday> Birthdays { get; set; }
    }
}
